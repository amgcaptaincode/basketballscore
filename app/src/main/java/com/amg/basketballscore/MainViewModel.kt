package com.amg.basketballscore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private var _totalLocalLiveData: MutableLiveData<Int> = MutableLiveData()
    private var _totalVisitorLiveData: MutableLiveData<Int> = MutableLiveData()

    val totalLocalLiveData: LiveData<Int> get() = _totalLocalLiveData

    val totalVisitorLiveData: LiveData<Int> get() = _totalVisitorLiveData

    init {
        resetScores()
    }


    fun resetScores() {
        _totalLocalLiveData.value = 0
        _totalVisitorLiveData.value = 0
    }

    fun addPointsToScore(points: Int, isLocal: Boolean) {
        if (isLocal) {
            _totalLocalLiveData.value = _totalLocalLiveData.value?.plus(points)
        } else {
            _totalVisitorLiveData.value = _totalVisitorLiveData.value?.plus(points)
        }
    }

    fun decreaseTotalLocal() {
        if (_totalLocalLiveData.value!! > 0) {
            _totalLocalLiveData.value = _totalLocalLiveData.value?.minus(1)
        }
    }

    fun decreaseTotalVisitor() {
        if (_totalVisitorLiveData.value!! > 0) {
            _totalVisitorLiveData.value = _totalVisitorLiveData.value?.minus(1)
        }
    }

}