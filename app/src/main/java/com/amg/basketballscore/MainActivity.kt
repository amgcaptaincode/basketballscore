package com.amg.basketballscore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.amg.basketballscore.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding.mainViewModel = viewModel

            viewModel.totalLocalLiveData.observe(this, Observer { totalLocalValue ->
            binding.tvTotalLocal.text = totalLocalValue.toString()
        })

        viewModel.totalVisitorLiveData.observe(this, Observer { totalVisitorValue ->
            binding.tvTotalVisitor.text = totalVisitorValue.toString()
        })

        setupButtons()

    }

    private fun setupButtons() {
        /*binding.btnMinusOneLocal.setOnClickListener {
            viewModel.decreaseTotalLocal()
        }

        binding.btnPlusOneLocal.setOnClickListener {
            addPointsToScore(1, true)
        }

        binding.btnPlusTwoLocal.setOnClickListener {
            addPointsToScore(2, true)
        }

        binding.btnMinusOneVisitor.setOnClickListener {
            viewModel.decreaseTotalVisitor()
        }

        binding.btnPlusOneVisitor.setOnClickListener {
            addPointsToScore(1, false)
        }

        binding.btnPlusTwoVisitor.setOnClickListener {
            addPointsToScore(2, false)
        }

        binding.btnReset.setOnClickListener {
            viewModel.resetScores()
        }*/

        binding.imgBtnGoTo.setOnClickListener {
            startScoreActivity()
        }
    }

    private fun addPointsToScore(points: Int, isLocal: Boolean) {
        viewModel.addPointsToScore(points, isLocal)
    }

    private fun startScoreActivity() {
        val intent = Intent(this, ScoreActivity::class.java)
        intent.putExtra(ScoreActivity.TOTAL_LOCAL, viewModel.totalLocalLiveData.value)
        intent.putExtra(ScoreActivity.TOTAL_VISITOR, viewModel.totalVisitorLiveData.value)
        startActivity(intent)
    }

}