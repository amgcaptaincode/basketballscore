package com.amg.basketballscore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.amg.basketballscore.databinding.ActivityScoreBinding

class ScoreActivity : AppCompatActivity() {

    companion object {

        val TOTAL_LOCAL = "total_local"
        val TOTAL_VISITOR = "total_visitor"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityScoreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val extras = intent.extras

        if (extras != null) {
            val totalLocal = extras.getInt(TOTAL_LOCAL)
            val totalVisitor = extras.getInt(TOTAL_VISITOR)

            binding.tvResult.text = getString(R.string.label_total, totalLocal, totalVisitor)

            when {
                totalLocal > totalVisitor -> binding.tvResultText.text = getString(R.string.label_local_team_won)
                totalVisitor > totalLocal -> binding.tvResultText.text = getString(R.string.label_visitors_won)
                else -> binding.tvResultText.text = getString(R.string.label_tie)
            }

        }

    }
}